import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/add/operator/do';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  constructor(
    private router: Router
  ) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    if (currentUser === null) {
      return next.handle(request).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {}
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401 || err.status === 0) {
            // localStorage.removeItem('currentUser');
            // this.router.navigate(['/']);
          }
        }
      });
    }

    const customReq = request.clone({
      headers: request.headers.set('Content-Type', 'application/json')
        .set('Authorization', 'JWT ' + currentUser.token)
    });

    return next.handle(customReq).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {}
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401 || err.status === 0) {
          localStorage.removeItem('currentUser');
          this.router.navigate(['/login']);
        }
      }
    });
  }
}
