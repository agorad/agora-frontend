import { User } from './user.model';

export class Auth {
    token: string;
    user: Array<User>;
    firstname: string;
    expires: Date;
}
