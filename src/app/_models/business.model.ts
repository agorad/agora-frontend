import { User } from './user.model';
import { Image } from './image.model';

export class Business {
    id: number;
    name: string;
    description: string;
    user: User;
    category: string;
    location: string;
    website: string;
    phone: string;
    opening_time: string;
    closing_time: string;
    slug: string;
    created_at: string;
    image: Image[];
}
