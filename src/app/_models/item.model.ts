import { Business } from './business.model';

export class Item {
    id: number;
    name: string;
    business: Business;
    description: string;
    extra_info: string;
    politics: string;
    rating: number;
    price: number;
    timestamp: string;
    updated: string;
}
