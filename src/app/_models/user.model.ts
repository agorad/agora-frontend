export class User {
    pk: number;
    id: number;
    firstName: string;
    lastName: string;
    email: string;
}
