import { User } from './user.model';

export class UserProfile {
 ;
    user: User;
    phone?: string;
    gender?: string;
}