import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Auth } from '../_models/auth.model';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
};

@Injectable()
export class AuthService {
  userUrl = environment.apiUrl + 'v1/login';
  private headers = new HttpHeaders();
  public token: string;
  public user: number;
  public firstname: string;
  public expires: Date;

  constructor(
    private http: HttpClient
  ) { }

  login(email: string, password: string): Observable<Auth> {
    return this.http.post(this.userUrl, JSON.stringify({ email: email, password: password}), httpOptions)
      .map((response: Auth) => {
        const auth = new Auth();

        auth.token = response && response['token'];
        auth.user = response && response['user'];
        auth.firstname = response && response['firstname'];
        auth.expires = response && response['expires'];

        const currentUser = JSON.stringify({
          firstname: auth.firstname,
          user: auth.user,
          token: auth.token,
          expires: auth.expires
        });

        if (auth.token) {
          this.token = auth.token;
          localStorage.setItem('currentUser', currentUser);
        }

        return response;
      },
        err => {
          console.log('ocurrió un error');
        }
      );
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
