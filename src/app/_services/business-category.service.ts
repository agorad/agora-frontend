import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { BusinessCategory } from '../_models/business-category.model';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
};

@Injectable()
export class BusinessCategoryService {
  baseUrl = environment.apiUrl + 'v1/business_categories?limit=50';
  private headers = new HttpHeaders();
  public token: string;
  public user: number;
  public expires: Date;

  constructor(
    private http: HttpClient
  ) { }

  list(): Observable<any> {
    const apiListEndpoint = `${this.baseUrl}`;
    return this.http.get(apiListEndpoint);
  }
}
