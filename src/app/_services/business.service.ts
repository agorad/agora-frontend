import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Business } from '../_models/business.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class BusinessService {
  baseUrl = environment.apiUrl + 'v1/business';
  private headers = new HttpHeaders();
  public token: string;
  public user: number;
  public expires: Date;

  constructor(private http: HttpClient) {}

  list(): Observable<any> {
    const apiListEndpoint = `${this.baseUrl}`;
    return this.http.get(apiListEndpoint);
  }

  get(id?: number): Observable<Business> {
    const apiDetailEndpoint = `${this.baseUrl}/${id}`;
    return this.http.get<Business>(apiDetailEndpoint);
  }

  getBusinessByUserId(userId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}?user_id=${userId}`);
  }

  update(businessItem: Business): Observable<any> {
    const apiDetailEndpoint = `${this.baseUrl}/${businessItem.id}`;
    return this.http.put(apiDetailEndpoint, businessItem);
  }

  createBusiness(
    name: string,
    description: string,
    location: string,
    website: string,
    phone: number,
    openingTime: number,
    closingTime: number,
    category: number
  ): Observable<Business> {
    console.log(category);
    return this.http.post<Business>(this.baseUrl, {
      name: name,
      description: description,
      location: location,
      website: website,
      phone: phone,
      openingTime: openingTime,
      closingTime: closingTime,
      category: category
    }, httpOptions).map(
      response => {
        return response;
      },
      err => {
        console.log('Ocurrió un error');
      }
    );
  }
}
