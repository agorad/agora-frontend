import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaderResponse } from '@angular/common/http/src/response';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Auth } from '../_models/auth.model';
import { User } from '../_models/user.model';
import { Item } from '../_models/item.model';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
};

@Injectable()
export class ItemService {
  baseUrl = environment.apiUrl + 'v1/';
  private headers = new HttpHeaders();
  public token: string;
  public user: number;
  public expires: Date;

  constructor(
    private http: HttpClient
  ) { }

  list(): Observable<any> {
    const apiListEndpoint = `${this.baseUrl}items/`;
    return this.http.get(apiListEndpoint);
  }

  get(id?: number): Observable<Item> {
    const apiDetailEndpoint = `${this.baseUrl}items/${id}/`;
    return this.http.get<Item>(apiDetailEndpoint);
  }

  createItem(
    name: string,
    description: string,
    extra_info: string,
    politics: string,
    rating: number,
    price: number,
    timestamp: string,
    updated: string,

  ): Observable<Item> {
    return this.http.post<Item>(this.baseUrl, {}, httpOptions).
      map(response => {
        return response;
      },
      err => {
        console.log('Ocurrió un error');
      }
    );
  }
}

