import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaderResponse } from '@angular/common/http/src/response';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Auth } from '../_models/auth.model';
import { User } from '../_models/user.model';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
};

@Injectable()
export class RegisterService {
  userUrl = environment.apiUrl + 'v1/profiles';
  private headers = new HttpHeaders();
  public token: string;
  public user: number;
  public expires: Date;

  constructor(
    private http: HttpClient
  ) { }

  register(firstName: string, lastName: string, email: string, password: string, passwordConfirm: string): Observable<Auth> {
    return this.http.post(this.userUrl, JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      passwordConfirm: passwordConfirm
    }), httpOptions)
      .map((response: Response) => {
        const auth = new Auth();

        auth.token = response && response['token'];
        auth.user = response && response['user'];
        auth.expires = response && response['expires'];

        if (auth.token) {
          this.token = auth.token;
          localStorage.setItem('currentUser', JSON.stringify({ email: email, token: auth.token, expires: auth.expires }));
        }

        return auth;
      },
        err => {
          console.log('ocurrió un error');
        }
      );
  }
}
