import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaderResponse } from '@angular/common/http/src/response';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { Auth } from '../_models/auth.model';
import { User } from '../_models/user.model';
import { Item } from '../_models/item.model';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
};

@Injectable()
export class UserProfileService {
  baseUrl = environment.apiUrl + 'v1/';
  private headers = new HttpHeaders();
  public token: string;
  public user: string;
  public expires: Date;

  constructor(
    private http: HttpClient
  ) { }

  // getItems(): Observable<Item> {
  //   return this.http.get(this.userUrl, httpOptions)
  //     .map((response: Response) => {
  //       return new Item();
  //     },
  //     err => {
  //         console.log('ocurrió un error');
  //       }
  //     );
  // }
  // list():Observable<any>{
  //   let apiListEndpoint= `${this.baseUrl}users/`
  //   return this.http.get(apiListEndpoint)
  // }

  get(id?: number): Observable<any>{
      let apiDetailEndpoint = `${this.baseUrl}userprofile/${id}/`
      return this.http.get<any>(apiDetailEndpoint)
  }
}
