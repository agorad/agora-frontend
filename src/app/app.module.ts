import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { HttpClient, HttpHeaders, HttpHandler, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpRequestInterceptor } from './_interceptors/http-request.interceptor';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { CookieService } from 'ngx-cookie-service';
import { RegisterService } from './_services/register.service';
import { AuthService } from './_services/auth.service';
import { BusinessService } from './_services/business.service';
import { ItemService } from './_services/item.service';
import { UserProfileService } from './_services/userprofile.service';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { BusinessListComponent } from './business-list/business-list.component';
import { ItemListComponent } from './item-list/item-list.component';
import { BusinessFormComponent } from './business-form/business-form.component';
import { VerifyAccountComponent } from './verify-account/verify-account.component';
import { BusinessDetailComponent } from './business-detail/business-detail.component';
import { AuthLogoutComponent } from './auth-logout/auth-logout.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { BusinessEditComponent } from './business-edit/business-edit.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ItemFormComponent } from './item-form/item-form.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { BusinessCategoryService } from './_services/business-category.service';
import { MapComponent } from './map/map.component';
import { AddressFormComponent } from './address-form/address-form.component';
import { CurrentLocationComponent } from './current-location/current-location.component';

import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { ImageUploadModule } from 'angular2-image-upload';

const appRoutes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginFormComponent },
  { path: 'register', component: RegisterFormComponent },
  { path: 'business/:id', component: BusinessDetailComponent },
  { path: 'business-form', component: BusinessFormComponent },
  { path: 'business-list', component: BusinessListComponent },
  { path: 'verify-account', component: VerifyAccountComponent },
  { path: 'item-list', component: ItemListComponent},
  { path: 'item/:id', component: ItemDetailComponent },
  { path: 'user/:id', component: UserProfileComponent },
  { path: 'item-form', component: ItemFormComponent },
  { path: 'business/:id/edit', component: BusinessEditComponent },
  { path: 'map', component: MapComponent },
  { path: 'current-location', component: CurrentLocationComponent },
  { path: 'address-form', component: AddressFormComponent},


];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginFormComponent,
    RegisterFormComponent,
    BusinessListComponent,
    ItemListComponent,
    BusinessFormComponent,
    VerifyAccountComponent,
    BusinessDetailComponent,
    AuthLogoutComponent,
    ItemDetailComponent,
    BusinessEditComponent,
    UserProfileComponent,
    ItemFormComponent,
    NavBarComponent,
    MapComponent,
    AddressFormComponent,
    CurrentLocationComponent,
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
    ReactiveFormsModule,
    GooglePlaceModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyBuOR3P2F7pYiYoHO1N0moFtBLg2ToL9fc'}),
   
    NgbModule.forRoot()
    // ImageUploadModule.forRoot(),
  ],
  providers: [
    CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
    HttpClient,
    RegisterService,
    AuthService,
    BusinessService,
    ItemService,
    UserProfileService,
    BusinessCategoryService,
    GoogleMapsAPIWrapper
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
