import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BusinessService } from '../_services/business.service';
import { Business } from '../_models/business.model';

@Component({
  selector: 'app-business-detail',
  templateUrl: './business-detail.component.html',
  styleUrls: ['./business-detail.component.css']
})

export class BusinessDetailComponent implements OnInit {
	detailId: any;
	businessItem: Business;
	
	constructor(
		private statusAPI: BusinessService,
  	private  location:Location,
  	private route: ActivatedRoute,
  	) { }

  ngOnInit() {
		this.detailId = this.route.snapshot.paramMap.get('id');
		let number = parseInt(this.detailId);
			
		if (!number) {
			this.goBack();
		} else {
			this.statusAPI.get(number).subscribe(data=>{
				console.log(data);
				this.businessItem = data as Business
			})
		}
	}

  goBack() {
		this.location.back();
  }
}

