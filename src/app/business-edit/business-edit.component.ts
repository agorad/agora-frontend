import { Component, EventEmitter,  OnInit, OnDestroy, Input, Output} from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';


import { AuthService } from '../_services/auth.service';
import { BusinessService } from '../_services/business.service';
import { Business } from '../_models/business.model'; 

import { Router } from '@angular/router';
@Component({
  selector: 'app-business-edit',
  templateUrl: './business-edit.component.html',
  styleUrls: ['./business-edit.component.css']
})
export class BusinessEditComponent implements OnInit {
	currentUser: any;
  businessForm: FormGroup;
  name: FormControl;
  viewEditForm:boolean = false;
  businessDir: NgForm;
  @Input() businessItem: Business;
  count = 0
  @Output() statusUpdated = new EventEmitter<Business>();
  businessUpdateSub: any;

  isUserOwner = false;

  constructor(
    private authenticationService: AuthService,
    private businessAPI: BusinessService,
    private router: Router
  ) {
    this.currentUser = authenticationService.getCurrentUser();
  }

  ngOnInit() {

    if (this.businessItem){
      let objOwner = this.businessItem.user
      let loggedUser =this.authenticationService.getCurrentUser().user
      if (objOwner == loggedUser){
        this.isUserOwner = true
      }

    }
  }

  getCurrentUser() {
    const currentUser = this.authenticationService.getCurrentUser();
    return currentUser;
  }

handleSubmit(event:any, businessDir:NgForm, businessForm:FormGroup){
      event.preventDefault()
      this.businessDir = businessDir
      if (businessDir.submitted){
          let submittedData = businessForm.value

          let newBusinessItem = new Business()
          newBusinessItem.id = this.businessItem.id
          newBusinessItem.name = businessForm.value.content
          newBusinessItem.image = null;

          this.businessUpdateSub = this.businessAPI.update(
            newBusinessItem
           ).subscribe(data=>{
             this.businessItem = data as Business
             this.statusUpdated.emit(this.businessItem)
           }, error=>{
             console.log(error)
           })

           this.toggleFormView()
      }
  }


  toggleFormView(){
    this.viewEditForm = !this.viewEditForm
  }

  buttonPressed(event){
      this.toggleFormView()
      this.count = this.count + 1
      // this.didUpdate.emit({count: this.count, status:this.statusId})
      // console.log(this.statusItem)
      if (this.businessItem){
          
         
          
          
      }
      
  }

}
