import { Component, OnInit } from '@angular/core';
import { BusinessCategoryService } from '../_services/business-category.service';
import { BusinessCategory } from '../_models/business-category.model';
import { BusinessService } from '../_services/business.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-business-form',
  templateUrl: './business-form.component.html',
  styleUrls: ['./business-form.component.css']
})

export class BusinessFormComponent implements OnInit {
  model: any = {};
  businessCategories: BusinessCategory[];

  constructor(
    private http: HttpClient,
    private router: Router,
    private businessService: BusinessService,
    private businessCategoryService: BusinessCategoryService
  ) { }

  ngOnInit() {
    this.getBusinessCategories();
  }

  getBusinessCategories() {
    this.businessCategoryService.list().subscribe(data => {
      this.businessCategories = data.results;
    });
  }

  createBusiness() {
    this.businessService.createBusiness(
      this.model.name,
      this.model.description,
      this.model.location,
      this.model.website,
      this.model.phone,
      this.model.openingTime,
      this.model.closingTime,
      this.model.category
    ).subscribe(result => {
      if (result != null) {
          this.router.navigate(['/home']);
        }
    }, error => {
      console.log('Ha ocurrido un error');
    });
  }

  onSubmit() {
    console.log('Envío de formulario business form');
    this.createBusiness();
  }
}
