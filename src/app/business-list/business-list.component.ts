import { Component, OnInit, OnDestroy } from '@angular/core';
import { BusinessService } from '../_services/business.service';
import { Business } from '../_models/business.model';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.component.html',
  styleUrls: ['./business-list.component.css']
})

export class BusinessListComponent implements OnInit {
  results: Business[];
  businessList: any;

  constructor(private businessService: BusinessService) {}

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.businessList = this.businessService.list().subscribe(data => {
      this.results = data.results;
    });
  }
}
