import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: any;

  constructor(
    private authenticationService: AuthService,
    private router: Router
  ) {
    this.currentUser = authenticationService.getCurrentUser();
  }

  ngOnInit() {
  }

  getCurrentUser() {
    const currentUser = this.authenticationService.getCurrentUser();
    return currentUser;
  }

  logOut() {
    this.authenticationService.logout();
  }
}
