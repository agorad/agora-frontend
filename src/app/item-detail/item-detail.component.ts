import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ItemService } from '../_services/item.service';
import { Item } from '../_models/item.model';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})

export class ItemDetailComponent implements OnInit {
  detailId: any;
  itemItem: Item;
  constructor(
    private statusAPI: ItemService,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.detailId = this.route.snapshot.paramMap.get('id');
    const number = parseInt(this.detailId);

    if (!number) {
      this.goBack();
    } else {
      this.statusAPI.get(number).subscribe(data => {
        console.log(data);
        this.itemItem = data as Item;
      });
    }
  }
  goBack() {
    this.location.back();
  }
}
