import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from '../_services/item.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnInit {
  model: any = {};

  constructor(
    private http: HttpClient,
    private router: Router,
    private itemService: ItemService
  ) { }

  ngOnInit() {
  }
  createItem() {
    this.itemService.createItem(
      this.model.name,
      this.model.description,
      this.model.extra_info,
      this.model.politics,
      this.model.rating,
      this.model.price,
      this.model.timestamp,
      this.model.cupdated,
    ).subscribe(result => {
      console.log('Se ha creado el negocio');
    }, error => {
      console.log('Ha ocurrido un error');
    });
  }

  onSubmit() {
    console.log('Envío de formulario business form');
    this.createItem();
  }

}
