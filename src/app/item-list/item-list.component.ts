import { Component, OnInit, OnDestroy} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ItemService } from '../_services/item.service';
import { Item } from '../_models/item.model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  results: Item[] = [];

  constructor(
    private itemService: ItemService
  ) { }

  ngOnInit() {
    this.itemService.list().subscribe(data => {
      console.log(data.count);
      this.results = data.results;
    });
   }
}
