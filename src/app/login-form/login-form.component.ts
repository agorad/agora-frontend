import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../_services/auth.service';
import { BusinessService } from '../_services/business.service';
import { Auth } from '../_models/auth.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  model: any = {};
  loginFailed: boolean;
  user: Auth;

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService,
    private businessService: BusinessService
  ) { }

  login() {
    this.authService.login(this.model.email, this.model.password)
      .subscribe(result => {
        if (result != null) {
          this.getBusinessByUser(result);
          this.router.navigate(['home/']);
        }
      }, error => {
        if (error.status === 401) {
          this.loginFailed = true;
        }
      });
  }

  getBusinessByUser(user: Auth) {
    this.businessService.getBusinessByUserId(user.user[0].pk);
  }

  onSubmit() {
    this.login();
    this.businessService.getBusinessByUserId(this.user.user[0].pk);
  }
}
