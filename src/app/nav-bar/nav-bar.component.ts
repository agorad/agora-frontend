import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { Router } from '@angular/router';
import { BusinessService } from '../_services/business.service';

import { Business } from '../_models/business.model';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  currentUser: any;
  results: Business[] = [];
  businessForUser: Business;
  businessList: any;
  constructor(
    private authenticationService: AuthService,
    private router: Router,
    private businessService: BusinessService
  ) {
    this.currentUser = authenticationService.getCurrentUser();
  }

  ngOnInit() {
    this.getCurrentUser();
    this.getBusinessByUserId(this.currentUser.user[0].pk);
  }

  getData() {
    this.businessList = this.businessService.list().subscribe(data => {
      this.results = data.results;
    });
  }

  getCurrentUser() {
    const currentUser = this.authenticationService.getCurrentUser();
    return currentUser;
  }

  getBusinessByUserId(userId: number) {
    this.businessService.getBusinessByUserId(userId).subscribe(data => {
      this.businessForUser = data.results[0];
    });
  }

  logOut() {
    this.authenticationService.logout();
  }
}
