import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { RegisterService } from '../_services/register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  constructor(private registerService: RegisterService, private router: Router,
  ) { }

  registerForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    passwordConfirm: new FormControl()
  });

  ngOnInit() {
  }

  onSubmit() {
    const registerData = this.registerForm.value;
    this.registerService.register(
      registerData.firstName,
      registerData.lastName,
      registerData.email,
      registerData.password,
      registerData.passwordConfirm).subscribe(result => {
        if (result != null) {
          this.router.navigate(['/home']);
        }
      }, error => {
      });
  }
}
