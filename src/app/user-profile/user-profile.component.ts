import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserProfileService } from '../_services/userprofile.service';
import { UserProfile } from '../_models/userprofile.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
	detailId: any;
	userData: UserProfile;

constructor(
	private statusAPI: UserProfileService,
	private  location:Location,
	private route: ActivatedRoute,
	) { }

  ngOnInit() {
		this.detailId = this.route.snapshot.paramMap.get('id');
		let number = parseInt(this.detailId);
		
		if (!number) {
			this.goBack();
		} else {
			this.statusAPI.get(number).subscribe(data => {
				console.log(data);
				this.userData = data as UserProfile
			})
		}
	}
	
  goBack() {
		this.location.back();
	}
}